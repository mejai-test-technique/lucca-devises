﻿using Lucca.Entities.Currency;
using Lucca.Entities.Report;
using System.Globalization;
using System.Text;
using System.Text.RegularExpressions;

namespace Lucca.Convertors.Currency
{
    /// <summary>
    /// Validation de la structure du fichier fourni pour la conversion du montant vers la devise cible
    /// Si le fichier est valide alors renvoyer la structure objet du fichier
    /// </summary>
    public class CurrencyFileValidator
    {
        /// <summary>
        /// Objet représentant le fichier fourni
        /// </summary>
        public CurrencyFile CurrencyFile { get; internal set; } = new CurrencyFile();

        /// <summary>
        /// Rapport d'erreurs de validation du fichier fourni
        /// </summary>
        public ConversionReport ConversionReport { get; internal set; } = new ConversionReport();

        /// <summary>
        /// Vérifier la structure du fichier fourni pour la conversion du montant vers la devise cible
        /// </summary>
        /// <param name="filePath">Chemin vers le fichier contenant les informations necessaires pour la conversion</param>
        public void Run(string? filePath)
        {
            CurrencyFile = new CurrencyFile();
            ConversionReport = new ConversionReport();

            try
            {
                //Vérifier que le chemin vers le fichier est bien renseigné
                if (string.IsNullOrWhiteSpace(filePath))
                {
                    ConversionReport.AddError(ConversionErrorType.EmptyArgument);
                    return;
                }

                //Vérifier que le fichier existe
                if (!File.Exists(filePath))
                {
                    ConversionReport.AddError(ConversionErrorType.FileNotFound, $"Veuillez vérifier l'exactitude du chemin fourni. Le fichier {filePath} n'existe pas.");
                    return;
                }

                //Vérifier la structure du fichier et renvoyer l'entité représentant le fichier
                using StreamReader stream = new StreamReader(filePath, Encoding.UTF8);
                CheckFileStructureAndBuildCurrencyFile(stream);
            }
            catch (Exception ex)
            {
                ConversionReport.AddError(ConversionErrorType.Unknown, $"Une erreur inattendue est survenue lors de la vérification de la structure du fichier de conversion de devises : {ex}");
            }
        }

        /// <summary>
        /// Vérifier la structure du fichier et renvoyer l'entité représentant le fichier
        /// </summary>
        /// <param name="filePath">Chemin vers le fichier contenant les informations necessaires pour la conversion</param>
        public void CheckFileStructureAndBuildCurrencyFile(StreamReader stream)
        {
            using (Microsoft.VisualBasic.FileIO.TextFieldParser textFieldParser = new Microsoft.VisualBasic.FileIO.TextFieldParser(stream))
            {
                textFieldParser.TextFieldType = Microsoft.VisualBasic.FileIO.FieldType.Delimited;
                textFieldParser.Delimiters = new[] { ";" };
                textFieldParser.HasFieldsEnclosedInQuotes = true;

                string[]? fields;

                uint lineIndex = 1;
                while (!textFieldParser.EndOfData)
                {
                    fields = textFieldParser.ReadFields();

                    if (lineIndex == 1)
                    {
                        CheckFirstLine(fields);
                    }
                    else if (lineIndex == 2)
                    {
                        CheckSecondLine(fields);
                    }
                    else if (lineIndex >= 3)
                    {
                        CheckCurrencyExchangeLine(fields, lineIndex);
                    }

                    lineIndex++;
                }
            }

            //Vérifier que le nombre de taux de change renseigné dans la deuxième ligne est égale au nombre de taux de change fournis dans le fichier
            if (CurrencyFile.CurrencyExchangeLinesCount != CurrencyFile.Lines.Count())
                ConversionReport.AddError(ConversionErrorType.InvalidExchangeLinesCount, $"Le nombre de taux de change {CurrencyFile.CurrencyExchangeLinesCount} renseigné dans la deuxième ligne est différent du nombre de taux de change {CurrencyFile.Lines.Count()} fournis dans le fichier.");
        }

        #region vérifier que la première ligne respecte le format D1;M;D2

        /// <summary>
        /// Vérifier que la première ligne respecte le format D1;M;D2
        /// </summary>
        /// <param name="fields">Contenu de la première ligne</param>
        void CheckFirstLine(string[]? fields)
        {
            if (fields == null || !fields.Any())
            {
                ConversionReport.AddError(ConversionErrorType.FirstLine_Empty);
                return;
            }

            if (fields.Length != 3 || fields.Any(field => string.IsNullOrWhiteSpace(field)))
            {
                ConversionReport.AddError(ConversionErrorType.FirstLine_InvalidFormat);
                return;
            }

            //La devise initiale D1 dans laquelle le montant est affiché, sous la forme d'un code de 3 caractères
            string sourceCurrencyCode = fields[0];
            if (!IsValidCurrencyCode(sourceCurrencyCode))
                ConversionReport.AddError(ConversionErrorType.FirstLine_InvalidSourceCurrency, $"Le format de la devise initiale D1 {sourceCurrencyCode} n'est pas valide ou n'est pas reconnu selon la norme ISO4217");
            else CurrencyFile.SourceCurrencyCode = sourceCurrencyCode;

            //Le montant M dans cette devise initiale, sous la forme d'un nombre entier positif > 0
            string sourceCurrencyAmout = fields[1];
            if (uint.TryParse(sourceCurrencyAmout, NumberStyles.AllowTrailingWhite | NumberStyles.AllowLeadingWhite, CultureInfo.InvariantCulture, out uint amount)
                && amount > 0)
                CurrencyFile.SourceCurrencyAmout = amount;
            else
                ConversionReport.AddError(ConversionErrorType.FirstLine_InvalidAmount, $"Le format du montant M {sourceCurrencyAmout} dans la devise initiale n'est valide. Il faut qu'il soit un nombre entier positif > 0");

            //La devise cible D2 vers laquelle il veut convertir le montant, sous la forme d'un code de 3 caractères
            string targetCurrencyCode = fields[2];
            if (!IsValidCurrencyCode(targetCurrencyCode))
                ConversionReport.AddError(ConversionErrorType.FirstLine_InvalidTargetCurrency, $"Le format de la devise cible D2 {targetCurrencyCode} n'est pas valide ou n'est pas reconnu selon la norme ISO4217.");
            else CurrencyFile.TargetCurrencyCode = targetCurrencyCode;
        }

        #endregion vérifier que la première ligne respecte le format D1;M;D2

        #region vérifier que la deuxième ligne contient un nombre entier N indiquant le nombre de taux de change qui sont transmis dans le fichier

        /// <summary>
        /// Vérifier que la deuxième ligne contient un nombre entier N indiquant le nombre de taux de change qui sont transmis dans le fichier
        /// </summary>
        /// <param name="fields">Contenu de la deuxième ligne</param>
        void CheckSecondLine(string[]? fields)
        {
            if (fields == null || !fields.Any())
            {
                ConversionReport.AddError(ConversionErrorType.SecondLine_Empty);
                return;
            }

            if (fields.Length != 1)
            {
                ConversionReport.AddError(ConversionErrorType.SecondLine_InvalidLength);
                return;
            }

            //Vérifier qu'il s'agit d'un nombre entier
            string currencyExchangeLinesCount = fields[0];
            if (uint.TryParse(currencyExchangeLinesCount, NumberStyles.AllowTrailingWhite | NumberStyles.AllowLeadingWhite, CultureInfo.InvariantCulture, out uint linesCount)
                && linesCount > 0)
                CurrencyFile.CurrencyExchangeLinesCount = linesCount;
            else
                ConversionReport.AddError(ConversionErrorType.SecondLine_InvalidFormat, $"Le format du nombre N {currencyExchangeLinesCount} indiquant le nombre de taux de change qui sont transmis dans le fichier n'est valide. Il faut qu'il soit un nombre entier positif > 0.");
        }

        #endregion vérifier que la deuxième ligne contient un nombre entier N indiquant le nombre de taux de change qui sont transmis dans le fichier

        /// <summary>
        /// Vérifier la structure de la ligne définissant le taux de change
        /// </summary>
        /// <param name="fields">Contenu de la ligne</param>
        void CheckCurrencyExchangeLine(string[]? fields, uint lineIndex)
        {
            if (fields == null || !fields.Any())
            {
                ConversionReport.AddError(ConversionErrorType.ExchangeLine_Empty, $"La ligne de taux de change {lineIndex} du fichier est vide.");
                return;
            }

            if (fields.Length != 3 || fields.Any(field => string.IsNullOrWhiteSpace(field)))
            {
                ConversionReport.AddError(ConversionErrorType.ExchangeLine_InvalidFormat, $"La ligne {lineIndex} du fichier ne respecte pas le format DD;DA;T");
                return;
            }

            bool hasError = false;
            CurrencyExchangeLine currencyExchangeLine = new CurrencyExchangeLine();

            //La devise de départ DD sous la forme d'un code de 3 caractères
            string sourceCurrencyCode = fields[0];
            if (!IsValidCurrencyCode(sourceCurrencyCode))
            {
                hasError = true;
                ConversionReport.AddError(ConversionErrorType.ExchangeLine_InvalidSourceCurrency, $"Le format de la devise de départ DD {sourceCurrencyCode} n'est pas valide ou n'est pas reconnu selon la norme ISO4217.");
            }
            else currencyExchangeLine.SourceCurrencyCode = sourceCurrencyCode;

            //La devise d'arrivée DA sous la forme d'un code de 3 caractères
            string targetCurrencyCode = fields[1];
            if (!IsValidCurrencyCode(targetCurrencyCode))
            {
                hasError = true;
                ConversionReport.AddError(ConversionErrorType.ExchangeLine_InvalidTargetCurrency, $"Le format de la devise d'arrivée DA {targetCurrencyCode} n'est pas valide ou n'est pas reconnu selon la norme ISO4217.");
            }
            else currencyExchangeLine.TargetCurrencyCode = targetCurrencyCode;

            //Le taux de change T sous la forme d'un nombre à 4 décimales (avec un "." comme séparateur décimal) 
            string exchangeRate = fields[2];
            if (decimal.TryParse(exchangeRate, NumberStyles.AllowDecimalPoint | NumberStyles.AllowTrailingWhite | NumberStyles.AllowLeadingWhite, CultureInfo.InvariantCulture, out decimal rate)
                && rate > 0)
            {
                //Vérifier qu'il y a bien 4 décimales après le point 
                int decimalCount = BitConverter.GetBytes(decimal.GetBits(rate)[3])[2];

                if (decimalCount != 4)
                {
                    hasError = true;
                    ConversionReport.AddError(ConversionErrorType.ExchangeLine_InvalidExchangeRateFormat, $"Le format du taux de change T {exchangeRate} n'est valide. Il faut qu'il soit un nombre à 4 décimales avec un '.' comme séparateur décimal.");
                }
                else currencyExchangeLine.ExchangeRate = rate;
            }
            else
            {
                hasError = true;
                ConversionReport.AddError(ConversionErrorType.ExchangeLine_InvalidExchangeRateFormat, $"Le format du taux de change T {exchangeRate} n'est valide. Il faut qu'il soit un nombre à 4 décimales avec un '.' comme séparateur décimal.");
            }

            if (!hasError) CurrencyFile.Lines.Add(currencyExchangeLine);
        }

        /// <summary>
        /// La devise devrait être sous la forme d'un code de 3 caractères
        /// </summary>
        /// <param name="currencyCode">Code de la devise</param>
        /// <returns>Renvoyer true si le code de la devise est valide</returns>
        bool IsValidCurrencyCode(string currencyCode)
        {
            //Vérifier que la longueur de la chaîne est 3
            if (currencyCode.Length != 3) return false;

            //Vérifier qu'il s'agit uniquement que des lettres
            if (!Regex.IsMatch(currencyCode, @"^[a-zA-Z]+$")) return false;

            //Vérifier que le code de la devise est bien reconnu selon la norme ISO4217
            return CultureInfo
                .GetCultures(CultureTypes.SpecificCultures)
                .Select(culture => new RegionInfo(culture.Name))
                .Any(regionInfo => regionInfo != null && string.Equals(regionInfo.ISOCurrencySymbol, currencyCode, StringComparison.InvariantCultureIgnoreCase));
        }
    }
}
