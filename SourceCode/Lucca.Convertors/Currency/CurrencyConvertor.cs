﻿using Lucca.Entities.Currency;
using Lucca.Entities.Report;

namespace Lucca.Convertors.Currency
{
    public class CurrencyConvertor
    {
        /// <summary>
        /// Rapport d'erreurs de conversion
        /// </summary>
        public ConversionReport ConversionReport { get; internal set; } = new ConversionReport();

        /// <summary>
        /// Montant converti dans la devise cible
        /// </summary>
        public int ConvertedAmount { get; internal set; } = 0;

        /// <summary>
        /// Convertir le montant dans une devise initiale vers une devise cible
        /// </summary>
        /// <param name="currencyFile">Objet représentant le fichier fourni</param>
        public void Run(CurrencyFile? currencyFile)
        {
            ConversionReport = new ConversionReport();

            try
            {
                //Vérifier que la structure fournie n'est pas vide
                if (currencyFile == null || currencyFile.Lines == null || !currencyFile.Lines.Any())
                {
                    ConversionReport.AddError(ConversionErrorType.CurrencyFile_Empty);
                    return;
                }

                //Vérifier que la devise initiale existe dans une des lignes de taux de change
                if (!currencyFile.Lines.Any(line => string.Equals(currencyFile.SourceCurrencyCode, line.SourceCurrencyCode, StringComparison.InvariantCulture))
                    && !currencyFile.Lines.Any(line => string.Equals(currencyFile.SourceCurrencyCode, line.TargetCurrencyCode, StringComparison.InvariantCulture)))
                {
                    ConversionReport.AddError(ConversionErrorType.CurrencyFile_SourceCurrencyNotFoundInExchangeLines);
                }

                //Vérifier que la devise cible existe dans une des lignes de taux de change
                if (!currencyFile.Lines.Any(line => string.Equals(currencyFile.TargetCurrencyCode, line.SourceCurrencyCode, StringComparison.InvariantCulture))
                    && !currencyFile.Lines.Any(line => string.Equals(currencyFile.TargetCurrencyCode, line.TargetCurrencyCode, StringComparison.InvariantCulture)))
                {
                    ConversionReport.AddError(ConversionErrorType.CurrencyFile_TargetCurrencyNotFoundInExchangeLines);
                }
            }
            catch (Exception ex)
            {
                ConversionReport.AddError(ConversionErrorType.Unknown, $"Une erreur inattendue est survenue lors de la conversion vers la devise cible : {ex}");
            }

            //S'il y a des erreurs alors abandonner le process de conversion
            if (ConversionReport.HasErrors)
                return;

            //Note : On part du principe que la structure de currencyFile est précédement contrôlée par CurrencyFileValidator
            ProcessCurrencyConversion(currencyFile!);
        }

        /// <summary>
        /// Procéder à la conversion du montant dans la devise initiale vers la devise cible
        /// </summary>
        /// <param name="currencyFile">Objet représentant le fichier fourni</param>
        void ProcessCurrencyConversion(CurrencyFile currencyFile)
        {
            try
            {
                //Récupérer le plus court chemin possible pour la conversion
                CurrencyConversionPath? conversionPath = GetValidCurrencyConversionPath(currencyFile);
                if (conversionPath == null)
                {
                    ConversionReport.AddError(ConversionErrorType.CurrencyFile_NoPossiblePathToConvertAmount);
                    return;
                }

                ConvertedAmount = (int)Math.Round(conversionPath.ConvertedAmount, 0);
            }
            catch (Exception ex)
            {
                ConversionReport.AddError(ConversionErrorType.Unknown, $"Une erreur inattendue est survenue lors de la conversion vers la devise cible : {ex}");
            }
        }

        /// <summary>
        /// Retourner le taux de change contenant le montant converti dans la devise cible
        /// </summary>
        /// <param name="currencyFile">Objet représentant le fichier fourni</param>
        /// <returns>Le taux de change contenant le montant converti dans la devise cible</returns>
        CurrencyConversionPath? GetValidCurrencyConversionPath(CurrencyFile currencyFile)
        {
            List<CurrencyConversionPath> currencyConversionTrees = GetCurrencyConversionTrees(currencyFile.SourceCurrencyCode, currencyFile.SourceCurrencyAmout, currencyFile.TargetCurrencyCode, currencyFile.Lines);

            if (currencyConversionTrees.Any())
            {
                List<IEnumerable<CurrencyConversionPath>> validFlattenCurrencyConversionTrees = new List<IEnumerable<CurrencyConversionPath>>();
                foreach (CurrencyConversionPath currencyConversionTree in currencyConversionTrees)
                {
                    IEnumerable<IEnumerable<CurrencyConversionPath>> flattenCurrencyConversionTrees = GetFlattenCurrencyConversionTrees(currencyConversionTree);
                    foreach (IEnumerable<CurrencyConversionPath> flattenCurrencyConversionTree in flattenCurrencyConversionTrees)
                    {
                        if (flattenCurrencyConversionTree.Any() && string.Equals(flattenCurrencyConversionTree.LastOrDefault()?.TargetCurrencyCode, currencyFile.TargetCurrencyCode, StringComparison.InvariantCulture))
                        {
                            validFlattenCurrencyConversionTrees.Add(flattenCurrencyConversionTree);
                        }
                    }
                }

                if (validFlattenCurrencyConversionTrees.Any())
                {
                    //Récupérer l'arbre ayant le chemin le plus court de conversion vers la devise d'arrivée
                    var shortFlattenCurrencyConversionTree = validFlattenCurrencyConversionTrees.OrderBy(path => path.Count()).FirstOrDefault();

                    //Retourner le taux de change contenant le montant converti dans la devise cible 
                    if (shortFlattenCurrencyConversionTree != null && shortFlattenCurrencyConversionTree.Any())
                        return shortFlattenCurrencyConversionTree.LastOrDefault();
                }
            }

            return null;
        }

        /// <summary>
        /// Retourner la liste d'arbres de conversion possibles
        /// </summary>
        /// <param name="sourceCurrencyCode">Code de la devise source</param>
        /// <param name="sourceCurrencyAmout">Montant dans la devise source</param>
        /// <param name="targetCurrencyCode">Code de la devise cible</param>
        /// <param name="lines">Liste des lignes de taux de change</param>
        /// <returns>Liste d'arbres de conversion possibles</returns>
        List<CurrencyConversionPath> GetCurrencyConversionTrees(string sourceCurrencyCode, decimal sourceCurrencyAmout, string targetCurrencyCode, List<CurrencyExchangeLine> lines)
        {
            List<CurrencyConversionPath> currencyConversionTrees = new List<CurrencyConversionPath>();

            foreach (CurrencyExchangeLine? line in lines.Where(line => string.Equals(line.SourceCurrencyCode, sourceCurrencyCode, StringComparison.InvariantCulture) || string.Equals(line.TargetCurrencyCode, sourceCurrencyCode, StringComparison.InvariantCulture)))
            {
                CurrencyConversionPath path = new CurrencyConversionPath();
                path.Amount = sourceCurrencyAmout;

                if (string.Equals(line.SourceCurrencyCode, sourceCurrencyCode, StringComparison.InvariantCulture))
                {
                    path.SourceCurrencyCode = line.SourceCurrencyCode;
                    path.TargetCurrencyCode = line.TargetCurrencyCode;
                    path.ExchangeRate = line.ExchangeRate;
                }
                else if (string.Equals(line.TargetCurrencyCode, sourceCurrencyCode, StringComparison.InvariantCulture))
                {
                    path.SourceCurrencyCode = line.TargetCurrencyCode;
                    path.TargetCurrencyCode = line.SourceCurrencyCode;
                    path.ExchangeRate = line.InvertedExchangeRate;
                }

                if (!string.Equals(path.TargetCurrencyCode, targetCurrencyCode, StringComparison.InvariantCulture))
                {
                    List<CurrencyExchangeLine> sublines = lines.Where(sline => !(string.Equals(sline.SourceCurrencyCode, line.SourceCurrencyCode, StringComparison.InvariantCulture) && string.Equals(sline.TargetCurrencyCode, line.TargetCurrencyCode, StringComparison.InvariantCulture))).ToList();

                    if (sublines.Any())
                        GetCurrencyConversionSubTree(path.TargetCurrencyCode, path.ConvertedAmount, targetCurrencyCode, sublines, path).ToList();
                }

                currencyConversionTrees.Add(path);
            }

            return currencyConversionTrees;
        }

        /// <summary>
        /// Retourner la liste des sous-noeuds de conversion possibles
        /// </summary>
        /// <param name="sourceCurrencyCode">Code de la devise source</param>
        /// <param name="sourceCurrencyAmout">Montant dans la devise source</param>
        /// <param name="targetCurrencyCode">Code de la devise cible</param>
        /// <param name="lines">Liste des lignes de taux de change</param>
        /// <param name="path">Noeud racine/parent de conversion</param>
        /// <returns>Liste des sous-noeuds de conversion possibles</returns>
        IEnumerable<CurrencyConversionPath> GetCurrencyConversionSubTree(string sourceCurrencyCode, decimal sourceCurrencyAmout, string targetCurrencyCode, List<CurrencyExchangeLine> lines, CurrencyConversionPath path)
        {
            foreach (CurrencyExchangeLine? line in lines.Where(line => string.Equals(line.SourceCurrencyCode, sourceCurrencyCode, StringComparison.InvariantCulture) || string.Equals(line.TargetCurrencyCode, sourceCurrencyCode, StringComparison.InvariantCulture)))
            {
                CurrencyConversionPath subPath = new CurrencyConversionPath();
                subPath.Amount = sourceCurrencyAmout;

                if (string.Equals(line.SourceCurrencyCode, sourceCurrencyCode, StringComparison.InvariantCulture))
                {
                    subPath.SourceCurrencyCode = line.SourceCurrencyCode;
                    subPath.TargetCurrencyCode = line.TargetCurrencyCode;
                    subPath.ExchangeRate = line.ExchangeRate;
                }
                else if (string.Equals(line.TargetCurrencyCode, sourceCurrencyCode, StringComparison.InvariantCulture))
                {
                    subPath.SourceCurrencyCode = line.TargetCurrencyCode;
                    subPath.TargetCurrencyCode = line.SourceCurrencyCode;
                    subPath.ExchangeRate = line.InvertedExchangeRate;
                }

                if (!string.Equals(subPath.TargetCurrencyCode, targetCurrencyCode, StringComparison.InvariantCulture))
                {
                    List<CurrencyExchangeLine> sublines = lines.Where(sline => !(string.Equals(sline.SourceCurrencyCode, line.SourceCurrencyCode, StringComparison.InvariantCulture) && string.Equals(sline.TargetCurrencyCode, line.TargetCurrencyCode, StringComparison.InvariantCulture))).ToList();

                    if (sublines.Any())
                    {
                        List<CurrencyConversionPath> paths = GetCurrencyConversionSubTree(subPath.TargetCurrencyCode, subPath.ConvertedAmount, targetCurrencyCode, sublines, subPath).ToList();
                        if (paths != null && paths.Any()) path.Paths.AddRange(paths);
                    }
                }
                else
                {
                    path.Paths.Add(subPath);
                }

                yield return path;
            }
        }

        /// <summary>
        /// Convertir l'arbre de conversion vers plusieurs listes à plat
        /// </summary>
        /// <param name="rootPath">Noeud racine/parent de conversion</param>
        /// <returns>Plusieurs listes à plat</returns>
        IEnumerable<IEnumerable<CurrencyConversionPath>> GetFlattenCurrencyConversionTrees(CurrencyConversionPath rootPath)
        {
            if (rootPath.Paths != null && rootPath.Paths.Any())
            {
                foreach (CurrencyConversionPath path in rootPath.Paths)
                {
                    foreach (IEnumerable<CurrencyConversionPath> subPath in GetFlattenCurrencyConversionTrees(path))
                    {
                        yield return new[] { rootPath }.Concat(subPath);
                    }
                }
            }
            else
            {
                yield return new[] { rootPath };
            }
        }
    }
}