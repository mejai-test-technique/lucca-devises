﻿using Lucca.Entities.Report;

namespace Lucca.Convertors.Currency
{
    public class CurrencyConversionManager
    {
        /// <summary>
        /// Rapport d'erreurs de conversion du fichier fourni
        /// </summary>
        public ConversionReport ConversionReport { get; internal set; } = new ConversionReport();

        /// <summary>
        /// Montant converti dans la devise cible
        /// </summary>
        public int ConvertedAmount { get; internal set; } = 0;

        /// <summary>
        /// Process de conversion d'un montant dans la devise cible à partir d'un fichier de paramétrage
        /// </summary>
        /// <param name="args">Chemin vers le fichier de paramétrage</param>
        public void Run(string[]? args)
        {
            ConversionReport = new ConversionReport();

            try
            {
                //Vérifier que le chemin vers le fichier est bien renseigné
                if (args == null || !args.Any())
                {
                    ConversionReport.AddError(ConversionErrorType.EmptyArgument);
                    return;
                }

                string? filePath = args.FirstOrDefault();

                //Vérifier la structure du fichier
                CurrencyFileValidator currencyFileValidator = new CurrencyFileValidator();
                currencyFileValidator.Run(filePath);

                //Si erreur de validation alors renvoyer le rapport
                if (currencyFileValidator.ConversionReport.HasErrors)
                {
                    ConversionReport.AddErrorsRange(currencyFileValidator.ConversionReport.Errors);
                    return;
                }

                //Si pas d'erreur alors lancer le process de conversion
                CurrencyConvertor currencyConvertor = new CurrencyConvertor();
                currencyConvertor.Run(currencyFileValidator.CurrencyFile);

                //Si erreur lors de la conversion alors renvoyer le rapport
                if (currencyConvertor.ConversionReport.HasErrors)
                {
                    ConversionReport.AddErrorsRange(currencyConvertor.ConversionReport.Errors);
                    return;
                }

                //Si pas d'erreurs alors renvoyer le montant dans la devise cible
                ConvertedAmount = currencyConvertor.ConvertedAmount;
            }
            catch (Exception ex)
            {
                ConversionReport.AddError(ConversionErrorType.Unknown, $"Une erreur inattendue est survenue lors de la conversion du montant vers la devise cible : {ex}");
            }
        }
    }
}
