﻿namespace Lucca.Entities.Report
{
    public class ConversionError
    {
        public ConversionErrorType Type { get; set; }

        private string? _Message;
        public string Message
        {
            get
            {
                if (string.IsNullOrWhiteSpace(_Message))
                {
                    string ressourceKey = $"ConversionErrorType_{Type}";
                    _Message = ConversionReportRessources.ResourceManager.GetString(ressourceKey);
                    if (string.IsNullOrWhiteSpace(_Message))
                        _Message = Type.ToString();
                }

                return _Message;
            }
            set
            {
                _Message = value;
            }
        }

    }

    public enum ConversionErrorType
    {
        Unknown,
        EmptyArgument,
        FileNotFound,

        FirstLine_Empty,
        FirstLine_InvalidFormat,
        FirstLine_InvalidSourceCurrency,
        FirstLine_InvalidAmount,
        FirstLine_InvalidTargetCurrency,

        SecondLine_Empty,
        SecondLine_InvalidLength,
        SecondLine_InvalidFormat,

        ExchangeLine_Empty,
        ExchangeLine_InvalidFormat,
        ExchangeLine_InvalidSourceCurrency,
        ExchangeLine_InvalidTargetCurrency,
        ExchangeLine_InvalidExchangeRateFormat,

        InvalidExchangeLinesCount,

        CurrencyFile_Empty,
        CurrencyFile_SourceCurrencyNotFoundInExchangeLines,
        CurrencyFile_TargetCurrencyNotFoundInExchangeLines,
        CurrencyFile_NoPossiblePathToConvertAmount
    }
}
