﻿namespace Lucca.Entities.Report
{
    public class ConversionReport
    {
        /// <summary>
        /// Liste des erreurs
        /// </summary>
        public List<ConversionError> Errors { get; private set; } = new List<ConversionError>();

        /// <summary>
        /// Renvoyer s'il y a au moins une erreur 
        /// </summary>
        public bool HasErrors
        {
            get
            {
                return Errors.Any();
            }
        }

        /// <summary>
        /// Vérifier l'existence d'un type d'erreur
        /// </summary>
        /// <param name="errorType">Type d'erreur</param>
        public bool HasErrorType(ConversionErrorType errorType)
        {
            return HasErrors && Errors.Any(error => error.Type == errorType);
        }

        /// <summary>
        /// Ajouter une erreur
        /// </summary>
        /// <param name="errorType">Type d'erreur</param>
        /// <param name="message">Message d'erreur personnalisé</param>
        public void AddError(ConversionErrorType errorType, string message = "")
        {
            Errors.Add(new ConversionError { Type = errorType, Message = message });
        }

        /// <summary>
        /// Renvoyer le rapport d'erreurs sous forme d'une chaine de caractères
        /// </summary>
        public string GetStringReport()
        {
            return HasErrors ? string.Join(Environment.NewLine, Errors.Select(error => error.Message)) : string.Empty;
        }

        public void AddErrorsRange(List<ConversionError> errors)
        {
            if (errors != null && errors.Any())
            {
                foreach (ConversionError error in errors)
                {
                    Errors.Add(error);
                }
            }
        }
    }
}
