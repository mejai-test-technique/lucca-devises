﻿namespace Lucca.Entities.Currency
{
    public class CurrencyFile
    {
        /// <summary>
        /// Code de la devise à convertir sous la forme de 3 caratères 
        /// </summary>
        public string SourceCurrencyCode { get; set; } = string.Empty;

        /// <summary>
        /// Montant strictement positif dans la devise source à convertir vers la devise cible
        /// </summary>
        public uint SourceCurrencyAmout { get; set; }

        /// <summary>
        /// Code de la devise cible sous la forme de 3 caratères 
        /// </summary>
        public string TargetCurrencyCode { get; set; } = string.Empty;

        /// <summary>
        /// Nombre de ligne de taux dans le fichier
        /// </summary>
        public uint CurrencyExchangeLinesCount { get; set; }

        /// <summary>
        /// Liste des lignes représentant les taux de change entre les différentes devises
        /// </summary>
        public List<CurrencyExchangeLine> Lines { get; set; } = new List<CurrencyExchangeLine>();
    }
}