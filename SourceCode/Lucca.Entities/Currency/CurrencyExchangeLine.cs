﻿namespace Lucca.Entities.Currency
{
    public class CurrencyExchangeLine
    {
        /// <summary>
        /// Code de la devise de départ sous la forme de 3 caratères 
        /// </summary>
        public string SourceCurrencyCode { get; set; } = string.Empty;

        /// <summary>
        /// Code de la devise d'arrivée sous la forme de 3 caratères 
        /// </summary>
        public string TargetCurrencyCode { get; set; } = string.Empty;

        /// <summary>
        /// Taux de change à 4 décimales
        /// </summary>
        public decimal ExchangeRate { get; set; }

        /// <summary>
        /// Taux de change inversé à 4 décimales
        /// </summary>
        public decimal InvertedExchangeRate
        {
            get
            {
                return ExchangeRate > 0 ? Math.Round(1 / ExchangeRate, 4) : 0;
            }
        }
    }
}
