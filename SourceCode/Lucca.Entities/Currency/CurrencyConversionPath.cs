﻿namespace Lucca.Entities.Currency
{
    public class CurrencyConversionPath
    {
        /// <summary>
        /// Code de la devise de départ sous la forme de 3 caratères 
        /// </summary>
        public string SourceCurrencyCode { get; set; } = string.Empty;

        /// <summary>
        /// Code de la devise d'arrivée sous la forme de 3 caratères 
        /// </summary>
        public string TargetCurrencyCode { get; set; } = string.Empty;

        /// <summary>
        /// Taux de change à 4 décimales
        /// </summary>
        public decimal ExchangeRate { get; set; }

        /// <summary>
        /// Montant dans la devise de départ
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// Montant dans la devise d'arrivée
        /// </summary>
        public decimal ConvertedAmount
        {
            get
            {
                return Math.Round(Amount * ExchangeRate, 4);
            }
        }

        /// <summary>
        /// Liste des paths permettant d'atteindre la devise d'arrivée en partant de la devise de départ
        /// </summary>
        public List<CurrencyConversionPath> Paths { get; set; } = new List<CurrencyConversionPath>();

        public override string ToString()
        {
            return $"{SourceCurrencyCode}||{TargetCurrencyCode}||{string.Format("{0:0.0000}", ExchangeRate)}||{string.Format("{0:0.0000}", Amount)}||{string.Format("{0:0.0000}", ConvertedAmount)}";
        }
    }
}
