﻿using Lucca.Convertors.Currency;
using Lucca.Entities.Currency;
using Lucca.Entities.Report;

namespace Lucca.Convertors.Tests.Currency
{
    public class CurrencyConvertorTest
    {
        CurrencyConvertor _CurrencyConvertor = new CurrencyConvertor();
        CurrencyFile _CurrencyFileCurrenciesNotFoundInExchangeLines = new CurrencyFile();
        CurrencyFile _CurrencyFileNoPossiblePath = new CurrencyFile();

        CurrencyFile _CurrencyFileSuccess = new CurrencyFile();

        [SetUp]
        public void SetUpCurrencyConvertor()
        {
            _CurrencyConvertor = new CurrencyConvertor();
        }

        [OneTimeSetUp]
        public void SetUpCurrencyFileCurrenciesNotFoundInExchangeLines()
        {
            _CurrencyFileCurrenciesNotFoundInExchangeLines = new CurrencyFile();
            _CurrencyFileCurrenciesNotFoundInExchangeLines.SourceCurrencyCode = "EUR";
            _CurrencyFileCurrenciesNotFoundInExchangeLines.TargetCurrencyCode = "JPY";
            _CurrencyFileCurrenciesNotFoundInExchangeLines.SourceCurrencyAmout = 550;
            _CurrencyFileCurrenciesNotFoundInExchangeLines.Lines.Add(new CurrencyExchangeLine()
            {
                SourceCurrencyCode = "AUD",
                TargetCurrencyCode = "CHF",
                ExchangeRate = (decimal)0.9661
            });
        }

        [OneTimeSetUp]
        public void SetUpCurrencyFileNoPossiblePath()
        {
            _CurrencyFileNoPossiblePath = new CurrencyFile();
            _CurrencyFileNoPossiblePath.SourceCurrencyCode = "EUR";
            _CurrencyFileNoPossiblePath.TargetCurrencyCode = "JPY";
            _CurrencyFileNoPossiblePath.SourceCurrencyAmout = 550;

            _CurrencyFileNoPossiblePath.CurrencyExchangeLinesCount = 2;

            _CurrencyFileNoPossiblePath.Lines.Add(new CurrencyExchangeLine() { SourceCurrencyCode = "JPY", TargetCurrencyCode = "KRW", ExchangeRate = (decimal)13.1151 });
            _CurrencyFileNoPossiblePath.Lines.Add(new CurrencyExchangeLine() { SourceCurrencyCode = "EUR", TargetCurrencyCode = "CHF", ExchangeRate = (decimal)1.2053 });
        }

        [OneTimeSetUp]
        public void SetUpSuccessCurrencyFile()
        {
            _CurrencyFileSuccess = new CurrencyFile();
            _CurrencyFileSuccess.SourceCurrencyCode = "EUR";
            _CurrencyFileSuccess.TargetCurrencyCode = "JPY";
            _CurrencyFileSuccess.SourceCurrencyAmout = 550;

            _CurrencyFileSuccess.CurrencyExchangeLinesCount = 6;

            _CurrencyFileSuccess.Lines.Add(new CurrencyExchangeLine() { SourceCurrencyCode = "AUD", TargetCurrencyCode = "CHF", ExchangeRate = (decimal)0.9661 });
            _CurrencyFileSuccess.Lines.Add(new CurrencyExchangeLine() { SourceCurrencyCode = "JPY", TargetCurrencyCode = "KRW", ExchangeRate = (decimal)13.1151 });
            _CurrencyFileSuccess.Lines.Add(new CurrencyExchangeLine() { SourceCurrencyCode = "EUR", TargetCurrencyCode = "CHF", ExchangeRate = (decimal)1.2053 });
            _CurrencyFileSuccess.Lines.Add(new CurrencyExchangeLine() { SourceCurrencyCode = "AUD", TargetCurrencyCode = "JPY", ExchangeRate = (decimal)86.0305 });
            _CurrencyFileSuccess.Lines.Add(new CurrencyExchangeLine() { SourceCurrencyCode = "EUR", TargetCurrencyCode = "USD", ExchangeRate = (decimal)1.2989 });
            _CurrencyFileSuccess.Lines.Add(new CurrencyExchangeLine() { SourceCurrencyCode = "JPY", TargetCurrencyCode = "INR", ExchangeRate = (decimal)0.6571 });
        }

        [Test]
        public void CurrencyFile_Empty()
        {
            _CurrencyConvertor.Run(null);

            Assert.IsTrue(_CurrencyConvertor.ConversionReport.HasErrorType(ConversionErrorType.CurrencyFile_Empty), "Il faut que l'erreur de type CurrencyFile_Empty soit retournée par le convertisseur de devises");
        }

        [Test]
        public void CurrencyFile_Empty_NoLines()
        {
            _CurrencyConvertor.Run(new CurrencyFile());

            Assert.IsTrue(_CurrencyConvertor.ConversionReport.HasErrorType(ConversionErrorType.CurrencyFile_Empty), "Il faut que l'erreur de type CurrencyFile_Empty soit retournée par le convertisseur de devises");
        }

        [Test]
        public void CurrencyFile_SourceCurrencyNotFoundInExchangeLines()
        {
            _CurrencyConvertor.Run(_CurrencyFileCurrenciesNotFoundInExchangeLines);

            Assert.IsTrue(_CurrencyConvertor.ConversionReport.HasErrorType(ConversionErrorType.CurrencyFile_SourceCurrencyNotFoundInExchangeLines), "Il faut que l'erreur de type CurrencyFile_SourceCurrencyNotFoundInExchangeLines soit retournée par le convertisseur de devises");
        }

        [Test]
        public void CurrencyFile_TargetCurrencyNotFoundInExchangeLines()
        {
            _CurrencyConvertor.Run(_CurrencyFileCurrenciesNotFoundInExchangeLines);

            Assert.IsTrue(_CurrencyConvertor.ConversionReport.HasErrorType(ConversionErrorType.CurrencyFile_TargetCurrencyNotFoundInExchangeLines), "Il faut que l'erreur de type CurrencyFile_TargetCurrencyNotFoundInExchangeLines soit retournée par le convertisseur de devises");
        }

        [Test]
        public void CurrencyFile_NoPossiblePathToConvertAmount()
        {
            _CurrencyConvertor.Run(_CurrencyFileNoPossiblePath);

            Assert.IsTrue(_CurrencyConvertor.ConversionReport.HasErrorType(ConversionErrorType.CurrencyFile_NoPossiblePathToConvertAmount), "Il faut que l'erreur de type CurrencyFile_NoPossiblePathToConvertAmount soit retournée par le convertisseur de devises");
        }

        [Test]
        public void CurrencyFile_ValidConvertAmount()
        {
            _CurrencyConvertor.Run(_CurrencyFileSuccess);

            Assert.IsFalse(_CurrencyConvertor.ConversionReport.HasErrors, "Il faut qu'aucune erreur soit renvoyée par le convertisseur de devises");
            Assert.That(_CurrencyConvertor.ConvertedAmount, Is.EqualTo(59033), "Il faut le montant dans la devise d'arrivée soit égale à 59033");
        }
    }
}
