﻿using Lucca.Convertors.Currency;
using Lucca.Entities.Report;
using System.Reflection;

namespace Lucca.Convertors.Tests.Currency
{
    public class CurrencyFileValidatorTest
    {
        CurrencyFileValidator _CurrencyFileValidator = new CurrencyFileValidator();

        [SetUp]
        public void SetUpCurrencyConvertor()
        {
            _CurrencyFileValidator = new CurrencyFileValidator();
        }

        [Test]
        public void EmptyArgument()
        {
            _CurrencyFileValidator.Run(null);

            Assert.IsTrue(_CurrencyFileValidator.ConversionReport.HasErrorType(ConversionErrorType.EmptyArgument), "Il faut que l'erreur de type EmptyArgument soit retournée par le convertisseur de devises");
        }

        [Test]
        public void FileNotFound()
        {
            _CurrencyFileValidator.Run("X::__//FileNotFound");

            Assert.IsTrue(_CurrencyFileValidator.ConversionReport.HasErrorType(ConversionErrorType.FileNotFound), "Il faut que l'erreur de type FileNotFound soit retournée par le convertisseur de devises");
        }

        #region first line tests

        [Test]
        public void FirstLine_InvalidFormat()
        {
            Assembly assembly = Assembly.GetExecutingAssembly();
            string resourceName = "Lucca.Convertors.Tests.Currency.ExampleFiles.FirstLine_InvalidFormat.txt";

            using (Stream? stream = assembly.GetManifestResourceStream(resourceName))
            {
                using StreamReader streamReader = new StreamReader(stream!);
                _CurrencyFileValidator.CheckFileStructureAndBuildCurrencyFile(streamReader);
            }

            Assert.IsTrue(_CurrencyFileValidator.ConversionReport.HasErrorType(ConversionErrorType.FirstLine_InvalidFormat), "Il faut que l'erreur de type FirstLine_InvalidFormat soit retournée par le convertisseur de devises");
        }

        [Test]
        public void FirstLine_InvalidSourceCurrency()
        {
            Assembly assembly = Assembly.GetExecutingAssembly();
            string resourceName = "Lucca.Convertors.Tests.Currency.ExampleFiles.FirstLine_InvalidSourceCurrency.txt";

            using (Stream? stream = assembly.GetManifestResourceStream(resourceName))
            {
                using StreamReader streamReader = new StreamReader(stream!);
                _CurrencyFileValidator.CheckFileStructureAndBuildCurrencyFile(streamReader);
            }

            Assert.IsTrue(_CurrencyFileValidator.ConversionReport.HasErrorType(ConversionErrorType.FirstLine_InvalidSourceCurrency), "Il faut que l'erreur de type FirstLine_InvalidSourceCurrency soit retournée par le convertisseur de devises");
        }

        [Test]
        public void FirstLine_InvalidAmount()
        {
            Assembly assembly = Assembly.GetExecutingAssembly();
            string resourceName = "Lucca.Convertors.Tests.Currency.ExampleFiles.FirstLine_InvalidAmount.txt";

            using (Stream? stream = assembly.GetManifestResourceStream(resourceName))
            {
                using StreamReader streamReader = new StreamReader(stream!);
                _CurrencyFileValidator.CheckFileStructureAndBuildCurrencyFile(streamReader);
            }

            Assert.IsTrue(_CurrencyFileValidator.ConversionReport.HasErrorType(ConversionErrorType.FirstLine_InvalidAmount), "Il faut que l'erreur de type FirstLine_InvalidAmount soit retournée par le convertisseur de devises");
        }

        [Test]
        public void FirstLine_InvalidTargetCurrency()
        {
            Assembly assembly = Assembly.GetExecutingAssembly();
            string resourceName = "Lucca.Convertors.Tests.Currency.ExampleFiles.FirstLine_InvalidTargetCurrency.txt";

            using (Stream? stream = assembly.GetManifestResourceStream(resourceName))
            {
                using StreamReader streamReader = new StreamReader(stream!);
                _CurrencyFileValidator.CheckFileStructureAndBuildCurrencyFile(streamReader);
            }

            Assert.IsTrue(_CurrencyFileValidator.ConversionReport.HasErrorType(ConversionErrorType.FirstLine_InvalidTargetCurrency), "Il faut que l'erreur de type FirstLine_InvalidTargetCurrency soit retournée par le convertisseur de devises");
        }

        #endregion first line tests

        #region second line tests

        [Test]
        public void SecondLine_InvalidLength()
        {
            Assembly assembly = Assembly.GetExecutingAssembly();
            string resourceName = "Lucca.Convertors.Tests.Currency.ExampleFiles.SecondLine_InvalidLength.txt";

            using (Stream? stream = assembly.GetManifestResourceStream(resourceName))
            {
                using StreamReader streamReader = new StreamReader(stream!);
                _CurrencyFileValidator.CheckFileStructureAndBuildCurrencyFile(streamReader);
            }

            Assert.IsTrue(_CurrencyFileValidator.ConversionReport.HasErrorType(ConversionErrorType.SecondLine_InvalidLength), "Il faut que l'erreur de type SecondLine_InvalidLength soit retournée par le convertisseur de devises");
        }

        [Test]
        public void SecondLine_InvalidFormat()
        {
            Assembly assembly = Assembly.GetExecutingAssembly();
            string resourceName = "Lucca.Convertors.Tests.Currency.ExampleFiles.SecondLine_InvalidFormat.txt";

            using (Stream? stream = assembly.GetManifestResourceStream(resourceName))
            {
                using StreamReader streamReader = new StreamReader(stream!);
                _CurrencyFileValidator.CheckFileStructureAndBuildCurrencyFile(streamReader);
            }

            Assert.IsTrue(_CurrencyFileValidator.ConversionReport.HasErrorType(ConversionErrorType.SecondLine_InvalidFormat), "Il faut que l'erreur de type SecondLine_InvalidFormat soit retournée par le convertisseur de devises");
        }

        #endregion second line tests

        #region exchange line tests

        [Test]
        public void ExchangeLine_InvalidFormat()
        {
            Assembly assembly = Assembly.GetExecutingAssembly();
            string resourceName = "Lucca.Convertors.Tests.Currency.ExampleFiles.ExchangeLine_InvalidFormat.txt";

            using (Stream? stream = assembly.GetManifestResourceStream(resourceName))
            {
                using StreamReader streamReader = new StreamReader(stream!);
                _CurrencyFileValidator.CheckFileStructureAndBuildCurrencyFile(streamReader);
            }

            Assert.IsTrue(_CurrencyFileValidator.ConversionReport.HasErrorType(ConversionErrorType.ExchangeLine_InvalidFormat), "Il faut que l'erreur de type ExchangeLine_InvalidFormat soit retournée par le convertisseur de devises");
        }

        [Test]
        public void ExchangeLine_InvalidSourceCurrency()
        {
            Assembly assembly = Assembly.GetExecutingAssembly();
            string resourceName = "Lucca.Convertors.Tests.Currency.ExampleFiles.ExchangeLine_InvalidSourceCurrency.txt";

            using (Stream? stream = assembly.GetManifestResourceStream(resourceName))
            {
                using StreamReader streamReader = new StreamReader(stream!);
                _CurrencyFileValidator.CheckFileStructureAndBuildCurrencyFile(streamReader);
            }

            Assert.IsTrue(_CurrencyFileValidator.ConversionReport.HasErrorType(ConversionErrorType.ExchangeLine_InvalidSourceCurrency), "Il faut que l'erreur de type ExchangeLine_InvalidSourceCurrency soit retournée par le convertisseur de devises");
        }

        [Test]
        public void ExchangeLine_InvalidTargetCurrency()
        {
            Assembly assembly = Assembly.GetExecutingAssembly();
            string resourceName = "Lucca.Convertors.Tests.Currency.ExampleFiles.ExchangeLine_InvalidTargetCurrency.txt";

            using (Stream? stream = assembly.GetManifestResourceStream(resourceName))
            {
                using StreamReader streamReader = new StreamReader(stream!);
                _CurrencyFileValidator.CheckFileStructureAndBuildCurrencyFile(streamReader);
            }

            Assert.IsTrue(_CurrencyFileValidator.ConversionReport.HasErrorType(ConversionErrorType.ExchangeLine_InvalidTargetCurrency), "Il faut que l'erreur de type ExchangeLine_InvalidTargetCurrency soit retournée par le convertisseur de devises");
        }

        [Test]
        public void ExchangeLine_InvalidExchangeRateFormat_NotDecimal()
        {
            Assembly assembly = Assembly.GetExecutingAssembly();
            string resourceName = "Lucca.Convertors.Tests.Currency.ExampleFiles.ExchangeLine_InvalidExchangeRateFormat_NotDecimal.txt";

            using (Stream? stream = assembly.GetManifestResourceStream(resourceName))
            {
                using StreamReader streamReader = new StreamReader(stream!);
                _CurrencyFileValidator.CheckFileStructureAndBuildCurrencyFile(streamReader);
            }

            Assert.IsTrue(_CurrencyFileValidator.ConversionReport.HasErrorType(ConversionErrorType.ExchangeLine_InvalidExchangeRateFormat), "Il faut que l'erreur de type ExchangeLine_InvalidExchangeRateFormat soit retournée par le convertisseur de devises");
        }

        [Test]
        public void ExchangeLine_InvalidExchangeRateFormat_Not4Digits()
        {
            Assembly assembly = Assembly.GetExecutingAssembly();
            string resourceName = "Lucca.Convertors.Tests.Currency.ExampleFiles.ExchangeLine_InvalidExchangeRateFormat_Not4Digits.txt";

            using (Stream? stream = assembly.GetManifestResourceStream(resourceName))
            {
                using StreamReader streamReader = new StreamReader(stream!);
                _CurrencyFileValidator.CheckFileStructureAndBuildCurrencyFile(streamReader);
            }

            Assert.IsTrue(_CurrencyFileValidator.ConversionReport.HasErrorType(ConversionErrorType.ExchangeLine_InvalidExchangeRateFormat), "Il faut que l'erreur de type ExchangeLine_InvalidExchangeRateFormat soit retournée par le convertisseur de devises");
        }

        #endregion exchange line tests

        [Test]
        public void InvalidExchangeLinesCount()
        {
            Assembly assembly = Assembly.GetExecutingAssembly();
            string resourceName = "Lucca.Convertors.Tests.Currency.ExampleFiles.InvalidExchangeLinesCount.txt";

            using (Stream? stream = assembly.GetManifestResourceStream(resourceName))
            {
                using StreamReader streamReader = new StreamReader(stream!);
                _CurrencyFileValidator.CheckFileStructureAndBuildCurrencyFile(streamReader);
            }

            Assert.IsTrue(_CurrencyFileValidator.ConversionReport.HasErrorType(ConversionErrorType.InvalidExchangeLinesCount), "Il faut que l'erreur de type InvalidExchangeLinesCount soit retournée par le convertisseur de devises");
        }
    }
}
