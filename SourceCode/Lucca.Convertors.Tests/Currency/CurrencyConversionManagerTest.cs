﻿using Lucca.Convertors.Currency;
using Lucca.Entities.Report;

namespace Lucca.Convertors.Tests.Currency
{
    public class CurrencyConversionManagerTest
    {
        [Test]
        public void EmptyArgument()
        {
            CurrencyConversionManager currencyConversionManager = new CurrencyConversionManager();
            currencyConversionManager.Run(null);

            Assert.IsTrue(currencyConversionManager.ConversionReport.HasErrorType(ConversionErrorType.EmptyArgument), "Il faut que l'erreur de type EmptyArgument soit retournée par le convertisseur de devises");
        }
    }
}
