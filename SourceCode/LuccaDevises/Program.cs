﻿using Lucca.Convertors.Currency;

namespace LuccaDevises
{
    /// <summary>
    /// Il s'agit d'un programme permettant de convertir un montant à partir d'une devise source vers une devise cible
    /// Le programme se base sur une liste de taux de change pour calculer le montant pour la devise cible
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            CurrencyConversionManager currencyConversionManager = new CurrencyConversionManager();
            currencyConversionManager.Run(args);
            if (currencyConversionManager.ConversionReport.HasErrors)
            {
                Console.WriteLine(currencyConversionManager.ConversionReport.GetStringReport());
                Environment.ExitCode = -1;
            }
            else
            {
                Console.WriteLine(currencyConversionManager.ConvertedAmount);
                Environment.ExitCode = 0;
            }
        }
    }
}