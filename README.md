# Lucca Devises

## Description

Il s'agit d'un programme permettant la conversion automatique d'un montant dans une devise de départ vers une devise d'arrivée.<br/>
Pour atteindre cet object, le programme se base sur un fichier respectant la structure suivante : <br/>
1. **Une première ligne** ayant le format D1;M;D2 :
    - D1 représente le code de trois caractères de la devise initiale
    - M est un entier strictement positif indiquant le montant dans la devise initiale
    - D2 représente le code de trois caractères de la devise d'arrivée

2. **Une deuxième ligne** contenant un nombre entier N précisant le nombre de lignes de taux de change transmises

3. **N lignes** de taux de change sous le format DD;DA;T :
    - DD représente le code de trois caractères de la devise de départ
    - DA représente le code de trois caractères de la devise d'arrivée
    - T est un nombre à 4 décimales indiquant le taux de change (avec '.' comme séparateur décimal)

## Installation

Pour récupérer l'application et le code source, il suffit de cloner le dépôt en utilisant cette commande :<br/>

```
git clone https://gitlab.com/mejai-test-technique/lucca-devises.git
```

Ensuite, lancer l'application **LuccaDevises.exe** sous le répertoire **Application** depuis la ligne de commande en précisant le chemin vers le fichier de paramétrage.<br/>
Un fichier **exemple.txt** est fourni avec l'application.<br/><br/>
Le code source **C#** de l'application est sous le répertoire **SourceCode**.<br/>
Le projet principal de l'application est **LuccaDevises**.

## Choix techniques

L'application est composée principalement :
 1. Un **validateur** de fichier permettant de vérifier que le fichier fourni respecte bien le format attendu<br/>
 2. Un **convertisseur** de devises qui converti automatiquement le montant fourni vers la devise d'arrivée<br/>
 Si plusieurs chemins sont possibles alors la conversion se fait via le chemin le plus court<br/>
 Que ce soit durant l'étape de validation ou de conversion, un rapport d'erreurs est renvoyé détaillant les erreurs de structure ou l'impossibilité de trouver un chemin de conversion.<br/>
 3. Un projet de **tests unitaires** pour s'assurer du bon fonctionnement du validateur et du convertisseur et que tous les cas d'erreurs sont bien gérés.
